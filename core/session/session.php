<?php

namespace core\session;

class session{

	public static function token()
	{
		$token = bin2hex(mhash(MHASH_TIGER160, random_bytes(18)));
		$_SESSION['_token'] = $token;
		return $token;
	}

	public static function verifyToken()
	{
		if ($_SERVER["REQUEST_METHOD"]==="POST") {
			if (!isset($_POST['_token'])) {
				http_response_code(400);
				echo "<h2 style='color:red; text-align:center;'>"."Invalid csrf Token!"."<h2>";
				die();
			}else{
					if ($_SESSION['_token']!==$_POST['_token']) {
					http_response_code(400);
					echo "<h2 style='color:red; text-align:center;'>"."Invalid csrf Token!"."<h2>";
					die();					
				}
			}
		}
	}

}