<?php

namespace core\Router;

class Router{

	public function show($page)
	{
		if(file_exists("views/".$page.".php")){
			return require "views/".$page.".php";
		}
	}

	public static function requestMethod($type)
	{
		switch ($type) {
			case 'POST':
				if($_SERVER['REQUEST_METHOD']!==$type)
				{
					require 'views/errors/POST.php';
					exit();
				}
			break;
			case 'GET':
				if($_SERVER['REQUEST_METHOD']!==$type)
				{
					require 'views/errors/GET.php';
					exit();
				}			
			break;	
			
		}
	}

	public function route($url,$call)
	{
		
		$urls 		= explode('/', $url);
		$baseUrls 	= $urls[1];
		unset($urls[1]);
		unset($urls[0]);
		$baseUrls	= explode(" ", $baseUrls);
		$params 	= $urls;


		$htaccess_urls = $_GET['url'];
		$htaccess_urls = rtrim($htaccess_urls,"/");
		$htaccess_urls = explode('/', $htaccess_urls);
		$htaccess_base_url = $htaccess_urls[0];
		unset($htaccess_urls[0]);
		$htaccess_params = $htaccess_urls;
		
		foreach ($baseUrls as $baseUrl) {
			
			if ($baseUrl===$htaccess_base_url) {
				
				if (sizeof($params)!==0 AND sizeof($htaccess_params)!==0) {
					$params = array_combine($params, $htaccess_params);
					if (is_callable($call)) {
						
						call_user_func($call,$baseUrl,$params);
					}
					exit();
				}

				if (sizeof($params)===0 AND sizeof($htaccess_params)===0) {
					call_user_func($call,$baseUrl);
					exit();
				}

			}

		}

	}
	
}