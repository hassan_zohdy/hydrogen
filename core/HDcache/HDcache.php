<?php

namespace core\HDcache;

class HDcache{


	/* here you can set you options */
	public $user		= "root";
	public $pass 		= "root";
	public $host 		= "localhost";
	public $database	= "blog";
	public $path		= "tmp/";
	/* end */
	
	public function set($table)
	{
		require 'core/HDcache/config.php';
	}

	public function get($table)
	{
		if(file_exists($this->path."$table".".json"))
		{
			$open = file_get_contents($this->path."$table".".json");
			return json_decode($open);
		}else{
			return null;
		}
	}

	public function delete($table)
	{
		unlink($this->path."$table".".json");
	}

	public function ManuelCache($filename,$data)
	{
		file_put_contents($this->path.$filename.".json", json_encode($data,JSON_PRETTY_PRINT));
	}

	public function ManuelGet($filename)
	{
		return json_decode(file_get_contents($this->path.$filename.".json"));
	} 
		
	
}