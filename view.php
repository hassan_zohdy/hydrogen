<?php

class view{

	public function show($page)
	{
		if(file_exists("views/".$page.".php")){
			return require "views/".$page.".php";
		}
	}

}