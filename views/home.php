<!DOCTYPE html>
<html>
<head>
	<title>Welcome</title>
	<link href='https://fonts.googleapis.com/css?family=Titillium+Web:200italic' rel='stylesheet' type='text/css'>
	<style type="text/css">
	img{
		max-width: 5%;
	}
	body{
		font-family: 'Titillium Web', sans-serif;
		font-size: 72px;
		text-align: center;
		padding-top: 15%;
		color:#d3d3d3;
	}
	</style>
</head>
<body>
Hydrogen Framework <img src="https://s3-eu-west-1.amazonaws.com/eurp/Graphics/Technohumanist/thlogo.png">
</body>
</html>